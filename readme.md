# Nextlabs Assignment

# Live At
https://nextlabs-assignment.herokuapp.com/admin_facing/home/

## Local Setup

- Git clone: `git clone git@gitlab.com:nirmalasainsara/nextlabs-assignment.git
- Go to the project directory: `cd nextlabs-assignment`
- Setup virtual environment: `virtualenv -p python3 .` (do not forget to add the dot(.) in the end)
- Activate virtual environment: `source bin/activate`
- Install requirements: `pip3 install -r requirements.txt`
- Run migrations: `python3 manage.py makemigrations`, `python manage.py migrate`
- Run server using local settings: `python3 manage.py runserver`
- See it running on [localhost](http://127.0.0.1:8000/)

## Tasks and Solutions

1. Basic python Write a regex to extract all the numbers in black colour text.

   Solution: [regex/code.py](https://gitlab.com/nirmalasainsara/nextlabs-assignment/-/blob/master/regex/code.py)


2. Get the data from the sheet - and post it to the newly created model as it’s object.

   Solution: [admin_facing/management/commands/upload_data.py](https://gitlab.com/nirmalasainsara/nextlabs-assignment/-/blob/master/admin_facing/management/commands/upload_data.py)

   Note: To run this locally use the command `python manage.py upload_data` but before that you have to download your `credentials.json` as explained in [Google Sheets API](https://developers.google.com/sheets/api/quickstart/python#step_1_turn_on_the).
   After downloading that file move it to `nextlabs-assignment` directory.


## PROBLEM SET-3

A.  I will use Celery. yes it is reliable.Python Celery is free and open-source software. This quality may appeal to organizations who support the open-source ethos, or who want to save money in their IT budget.It’s so simple and lightweight, installing Python Celery is very easy.Celery supports multiple message brokers, including RabbitMQ and Redis. Amazon SQS is also supported, but lacks features such as monitoring and remote control.



B.  Flask is a microframework that is focused on simplicity. Minimalism and fine grained control.it implements the bare minimum,leaving the developer with complete freedom of choice in terms of modules and add-ons. Django adopts an all inclusive “batteries included” approach, providing an admin panel, ORM(Object Relational Mapping), database interfaces and directory structure straight out of the box.
