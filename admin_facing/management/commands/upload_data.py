import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from admin_facing.models import Application  # according to your modal
from django.core.management.base import BaseCommand

# If modifying these scopes, delete the file token.pickle.
SCOPES = ["https://www.googleapis.com/auth/spreadsheets.readonly"]

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = "14thWjndJCdA2Mwado9I2TJsQg8R4Gez0lejXO8eekX4"
SAMPLE_RANGE_NAME = "A4:D6"


class Command(BaseCommand):
    help = "upload data from google sheet"

    def handle(self, *args, **options):
        """
        Fetch data from google sheet using sheet API
        Create Employee Model object using that
        """
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists("token.pickle"):
            with open("token.pickle", "rb") as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    "./credentials.json", SCOPES
                )
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open("token.pickle", "wb") as token:
                pickle.dump(creds, token)

        service = build("sheets", "v4", credentials=creds)

        # Call the Sheets API
        sheet = service.spreadsheets()
        result = (
            sheet.values()
            .get(spreadsheetId=SAMPLE_SPREADSHEET_ID, range=SAMPLE_RANGE_NAME)
            .execute()
        )
        values = result.get("values", [])

        if not values:
            print("No data found.")
        else:
            print("uploading data...")
            for row in values:
                AppName = row[0]
                Category = row[1]
                Points = row[2]
                try:
                    # make changes according to your model
                    Application.objects.create(
                        AppName=AppName,
                        Category=Category,
                        Points=Points,
                    )
                except Exception as e:
                    print(e)
            print("done!")
