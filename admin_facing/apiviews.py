from rest_framework import generics, permissions
from rest_framework.response import Response
from .serializer import (
    UserSerializer,
    RegisterSerializer,
    AppDataSerializer,
    ApplistSerializer,
)
from rest_framework import status
from .models import Application, Task
from django.core.files.storage import FileSystemStorage
from rest_framework.decorators import api_view
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.models import User
from django.contrib import messages
from rest_framework.permissions import IsAuthenticated


class AppApi(generics.GenericAPIView):
    serializer_class = AppDataSerializer

    def get(self, request, *args, **kwargs):
        applications = Application.objects.all()
        serializer = AppDataSerializer(applications, many=True)
        response = {
            "applications": serializer.data,
        }
        return Response(response)

    def post(self, request, *args, **kwargs):
        serializer = AppDataSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        app = serializer.save()
        return Response(
            {
                "user": AppDataSerializer(app).data,
                "message": "app Created Successfully.",
            }
        )


class TaskCreateApi(generics.GenericAPIView):
    serializer_class = ApplistSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        app = Application.objects.get(id=kwargs.get("app_id"))
        # if serializer.is_valid(raise_exception=True):
        img = request.FILES.get("image")
        fs = FileSystemStorage()
        image = fs.save(img.name, img)
        qs = Task.objects.filter(user=request.user, application=app)
        if qs.exists():
            messages.warning(request, "user already exists.")
        task = Task.objects.create(user=request.user, application=app, image=image)
        return Response(
            {
                "img": ApplistSerializer(task).data,
                "message": "task Created Successfully.",
            }
        )
